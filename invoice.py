# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from trytond.model import Model, fields
from trytond.modules.currency.fields import Monetary
from trytond.modules.product import price_digits
from trytond.pool import PoolMeta
from trytond.pyson import Bool, Eval


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    amount_with_cost = fields.Function(
        Monetary('Amount with cost', currency='currency', digits='currency'),
        'get_amount_with_cost')
    unit_price_with_cost = fields.Function(
        Monetary('Unit price with cost', currency='currency',
            digits=price_digits),
        'get_unit_price_with_cost')
    unit_price_readonly = fields.Function(
        fields.Boolean('Unit price readonly'),
        'get_unit_price_readonly')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        if cls.unit_price.states.get('readonly'):
            cls.unit_price.states['readonly'] |= Bool(
                Eval('unit_price_readonly'))
        else:
            cls.unit_price.states['readonly'] = Bool(
                Eval('unit_price_readonly'))

    def get_amount_with_cost(self, name):
        if self.type == 'line':
            return self.currency.round(
                Decimal(self.quantity) * self.unit_price_with_cost)

    def get_unit_price_with_cost(self, name):
        if (self.origin
                and isinstance(self.origin, Model)
                and self.origin.__name__ == 'sale.line'):
            return self.origin.unit_price_with_cost
        return self.unit_price

    def get_unit_price_readonly(self, name):
        return self._has_sale_unit_price_cost

    @classmethod
    def _get_origin(cls):
        return super()._get_origin() + ['sale.cost']

    @property
    def _has_sale_unit_price_cost(self):
        return bool(self.origin
            and isinstance(self.origin, Model)
            and self.origin.__name__ == 'sale.line'
            and self.origin.sale_price_cost_lines)
