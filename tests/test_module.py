# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class SaleCostTestCase(ModuleTestCase):
    """Test Sale cost module"""
    module = 'sale_cost'
    extras = ['analytic_sale']


del ModuleTestCase
