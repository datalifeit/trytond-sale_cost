=====================================
Sale Cost Apply Invoice Sale Scenario
=====================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts, create_tax
    >>> today = datetime.date.today()


Install sale_cost_apply_invoice::

    >>> config = activate_modules(['sale_cost', 'analytic_sale'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> expense = accounts['expense']
    >>> revenue = accounts['revenue']
    >>> revenue2, = revenue.duplicate()
    >>> revenue2.name = 'Revenue 2'
    >>> revenue2.save()


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()
    >>> tax2 = create_tax(Decimal('.15'))
    >>> tax2.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> Tax = Model.get('account.tax')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> tax = Tax(tax.id)
    >>> account_category.save()

    >>> account_category2 = ProductCategory(name="Account Category 2")
    >>> account_category2.accounting = True
    >>> account_category2.account_expense = expense
    >>> account_category2.account_revenue = revenue2
    >>> account_category2.customer_taxes.append(tax2)
    >>> tax2 = Tax(tax2.id)
    >>> account_category2.save()


Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer2 = Party(name='Customer 2')
    >>> customer2.save()


Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount()
    >>> root.name = 'Root'
    >>> root.code = '1'
    >>> root.type = 'root'
    >>> root.save()
    >>> analytic_account = AnalyticAccount()
    >>> analytic_account.name = 'Analytic 11'
    >>> analytic_account.code = '11'
    >>> analytic_account.root = root
    >>> analytic_account.parent = root
    >>> analytic_account.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product1 = template.products[0]

    >>> template2 = ProductTemplate()
    >>> template2.name = 'Product 2'
    >>> template2.default_uom = kg
    >>> template2.type = 'goods'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('10')
    >>> template2.cost_price = Decimal('5')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.account_category = account_category2
    >>> template2.save()
    >>> product2 = template2.products[0]


Create costs types::

    >>> CostType = Model.get('sale.cost.type')
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('30')
    >>> template.salable = True
    >>> template.account_category = account_category
    >>> template.save()
    >>> service = template.products[0]

    >>> cost_type = CostType(name='Cost Type Sale price')
    >>> cost_type.product = service
    >>> cost_type.formula = '-0.5*quantity'
    >>> cost_type.apply_method = 'sale'
    >>> cost_type.include_on_price = True
    >>> cost_type.manual = True
    >>> cost_type.save()

    >>> cost_type2 = CostType(name='Cost Type Entry invoice')
    >>> cost_type2.product = service
    >>> cost_type2.formula = '0.1*quantity'
    >>> cost_type2.apply_method = 'sale_invoice'
    >>> cost_type2.manual = True
    >>> cost_type2.save()

    >>> cost_type3 = CostType(name='Cost Type Sale invoice')
    >>> cost_type3.formula = '0.1*quantity'
    >>> cost_type3.apply_method = 'sale_invoice'
    >>> cost_type3.manual = True
    >>> cost_type3.use_product_account = True
    >>> cost_type3.save()


Create Sale with cost with apply method "Sale price"::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 500.0
    >>> sale_line.unit_price
    Decimal('10.0000')
    >>> cost = sale.costs.new()
    >>> cost.type_ = cost_type
    >>> sale.click('quote')
    >>> sale.costs[0].sale_invoice_state
    'none'
    >>> sale.click('confirm')
    >>> sale.costs[0].sale_invoice_state
    'pending'
    >>> len(sale.lines)
    2
    >>> sline1, = [l for l in sale.lines if l.product == product1]
    >>> sline2, = [l for l in sale.lines if l.product == service]
    >>> sline1.amount_with_cost == sline1.amount - sale.costs[0].amount
    True
    >>> sline1.unit_price_with_cost == sline1.amount_with_cost / Decimal(sline1.quantity)
    True
    >>> sline2.quantity
    1.0
    >>> sline2.unit_price
    Decimal('250.0000')
    >>> sline2.amount
    Decimal('250.00')


Check invoice lines::

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> lines = InvoiceLine.find([])
    >>> len(lines)
    2
    >>> iline1, = [l for l in lines if l.product == product1]
    >>> iline2, = [l for l in lines if l.product == service]
    >>> iline1.unit_price_with_cost == sline1.unit_price_with_cost
    True
    >>> iline1.amount_with_cost == Decimal(iline1.quantity) * iline1.unit_price_with_cost
    True
    >>> iline2.origin == sline2
    True
    >>> iline2.unit_price == iline2.unit_price
    True
    >>> iline2.amount == iline2.amount
    True
    >>> len(sale.costs[0].invoice_lines)
    0


Create sale with cost with apply method "Sale invoice"::

    >>> sale2 = Sale()
    >>> sale2.party = customer2
    >>> sale2.invoice_method = 'order'
    >>> sale_line = sale2.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 500.0
    >>> entry_acc, = sale_line.analytic_accounts
    >>> entry_acc.account = analytic_account
    >>> cost = sale2.costs.new()
    >>> cost.type_ = cost_type2
    >>> cost2 = sale2.costs.new()
    >>> cost2.type_ = cost_type2
    >>> cost2.formula = '0.05*quantity'
    >>> sale2.click('quote')
    >>> sale2.click('confirm')


Check invoice line::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice, = sale2.invoices
    >>> len(invoice.lines)
    3
    >>> ilines = [l for l in invoice.lines if l.product == cost_type2.product]
    >>> len(ilines)
    2
    >>> sale2.reload()
    >>> all(l.origin in sale2.costs for l in ilines)
    True
    >>> ilines[0].quantity == ilines[1].quantity == 1
    True
    >>> ilines[0].unit_price
    Decimal('-50.0000')
    >>> ilines[0].amount
    Decimal('-50.00')
    >>> ilines[1].unit_price
    Decimal('-25.0000')
    >>> ilines[1].amount
    Decimal('-25.00')
    >>> invoice.untaxed_amount
    Decimal('4925.00')
    >>> invoice.tax_amount
    Decimal('492.50')
    >>> len(sale2.costs[0].invoice_lines)
    1
    >>> entry_acc, = ilines[0].analytic_accounts
    >>> entry_acc.account == analytic_account
    True


Create sale with cost with apply method "Sale invoice" and use product account::

    >>> sale3 = Sale()
    >>> sale3.party = customer2
    >>> sale3.invoice_method = 'order'
    >>> sale_line = sale3.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 250.0
    >>> entry_acc, = sale_line.analytic_accounts
    >>> entry_acc.account = analytic_account
    >>> sale_line2 = sale3.lines.new()
    >>> sale_line2.product = product2
    >>> sale_line2.quantity = 200.0
    >>> entry_acc2, = sale_line2.analytic_accounts
    >>> entry_acc2.account = analytic_account
    >>> sale_line3 = sale3.lines.new()
    >>> sale_line3.product = product1
    >>> sale_line3.quantity = 150.0
    >>> entry_acc3, = sale_line3.analytic_accounts
    >>> entry_acc3.account = analytic_account
    >>> cost3 = sale3.costs.new()
    >>> cost3.type_ = cost_type3
    >>> sale3.click('quote')
    >>> sale3.click('confirm')


Check invoice line::

    >>> invoice, = sale3.invoices
    >>> len(invoice.lines)
    5
    >>> iline1, = [l for l in invoice.lines if l.product == None and l.account == revenue]
    >>> iline2, = [l for l in invoice.lines if l.product == None and l.account == revenue2]
    >>> iline1.description == cost_type3.name
    True
    >>> iline1.quantity
    1.0
    >>> iline1.unit_price
    Decimal('-40.0000')
    >>> iline1.amount
    Decimal('-40.00')
    >>> len(iline1.taxes)
    1
    >>> iline1.taxes == account_category.customer_taxes
    True
    >>> iline2.description == cost_type3.name
    True
    >>> iline2.quantity
    1.0
    >>> iline2.unit_price
    Decimal('-20.0000')
    >>> iline2.amount
    Decimal('-20.00')
    >>> len(iline2.taxes)
    1
    >>> iline2.taxes == account_category2.customer_taxes
    True