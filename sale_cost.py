# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from itertools import groupby

from sql import Literal, Null, Union
from sql.aggregate import Sum
from sql.conditionals import Coalesce
from sql.functions import Abs, Round
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import (DeactivableMixin, Index, MatchMixin, ModelSQL,
    ModelView, dualmethod, fields)
from trytond.modules.currency.fields import Monetary
from trytond.modules.document_cost.cost import (AccountMixin, cost_document,
    cost_document_line, cost_mixin, cost_type)
from trytond.modules.product import price_digits
from trytond.pool import Pool, PoolMeta
from trytond.pyson import And, Bool, Equal, Eval, If, In, Not
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard


class SaleCostApplyMethodMixin(object):
    __slots__ = ()

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.apply_method.selection.extend([
            ('sale', 'Sale'),
            ('sale_invoice', 'Sale invoice')])


class CostType(SaleCostApplyMethodMixin, AccountMixin, cost_type(), ModelSQL,
        ModelView, MatchMixin):
    """Sale Cost Type"""
    __name__ = 'sale.cost.type'

    apply_point = fields.Selection([
            ('on_confirm', 'On Sale Confirm'),
            ('manual', 'Manual')
        ], 'Apply point',
        domain=[
            If(In(Eval('apply_method'), ['sale', 'sale_invoice']),
                ('apply_point', '=', 'on_confirm'),
                ())
        ],
        states={
            'readonly': (Eval('apply_method') == 'sale'),
            'invisible': Eval('apply_method') == 'none',
        })
    products = fields.Many2Many('sale.cost.type-product.product',
        'cost_type', 'product', 'Products')
    include_on_price = fields.Boolean('Include on price',
        states={
            'invisible': ~Eval('apply_method').in_(['sale', 'sale_invoice'])
        })
    use_product_account = fields.Boolean('Use product account',
        states={
            'invisible': (Eval('apply_method') != 'sale_invoice')
        })

    @classmethod
    def __setup__(cls):
        super(CostType, cls).__setup__()
        cls.distribution_method.selection.append(('none', 'None'))
        cls.product.states['required'] &= Not(
            Bool(Eval('use_product_account')))
        cls.product.states['invisible'] |= Bool(Eval('use_product_account'))

        cls.account.states['required'] &= (
            (Eval('apply_method') != 'sale')
            & Not(Bool(Eval('use_product_account'))))
        cls.account.states['invisible'] |= Bool(Eval('use_product_account'))

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)
        sql_table = cls.__table__()
        cursor = Transaction().connection.cursor()
        cursor.execute(
            *sql_table.update(
                [sql_table.apply_method, sql_table.include_on_price],
                ['sale', Literal(True)
                    if sql_table.apply_method == 'sale_price'
                    else Literal(False)
                ],
                where=sql_table.apply_method.in_(['customer', 'sale_price']))
        )

    @staticmethod
    def default_apply_point():
        return 'on_confirm'

    @fields.depends('apply_method')
    def on_change_apply_method(self):
        if self.apply_method in ('sale', 'sale_invoice'):
            self.apply_point = 'on_confirm'
        elif self.apply_method != 'sale':
            self.include_on_price = False

    @fields.depends('use_product_account')
    def on_change_use_product_account(self):
        if self.use_product_account:
            self.product = None
            self.account = None

    def match(self, pattern):
        if 'products' in pattern:
            pattern = pattern.copy()
            products = pattern.pop('products')
            if (self.products and not (
                    set(p.id for p in self.products) & set(products))):
                return False
        return super().match(pattern)

    @classmethod
    def _account_domain(cls):
        InvoiceLine = Pool().get('account.invoice.line')
        res = super()._account_domain()
        return [
            ('sale_invoice', [
                ('kind', 'in', InvoiceLine._account_domain('out'))]),
        ] + res


class CostTemplate(DeactivableMixin, ModelSQL, ModelView, cost_mixin()):
    """Cost template"""
    __name__ = 'sale.cost.template'

    type_ = fields.Many2One('sale.cost.type', 'Type', required=True,
        ondelete='RESTRICT',
        states={'readonly': ~Eval('active')})
    party = fields.Many2One('party.party', 'Party',
        ondelete='RESTRICT',
        states={
            'readonly': ~Eval('active'),
            'required': Bool(Eval('shipment_party'))})
    shipment_address = fields.Many2One('party.address', 'Shipment Address',
        domain=[('party', '=', If(Bool(Eval('shipment_party')),
            Eval('shipment_party'), Eval('party')))],
        states={
            'readonly': ~Eval('active'),
            'invisible': ~Eval('party')})
    apply_method = fields.Function(fields.Selection(
            'get_apply_methods', 'Apply method'),
        'on_change_with_apply_method')
    shipment_party = fields.Many2One('party.party',
        'Shipment party',
        states={'invisible': Not(Eval('party'))})

    @classmethod
    def __setup__(cls):
        super(CostTemplate, cls).__setup__()
        cls.distribution_method.selection.append(('none', 'None'))
        for _field_name in ('formula', 'distribution_method'):
            _field = getattr(cls, _field_name, None)
            if _field.states.get('readonly'):
                _field.states['readonly'] |= ~Eval('active')
            else:
                _field.states['readonly'] = ~Eval('active')

    @classmethod
    def get_apply_methods(cls):
        Type = Pool().get('sale.cost.type')
        return Type.apply_method.selection

    @fields.depends('type_')
    def on_change_with_apply_method(self, name=None):
        if self.type_:
            return self.type_.apply_method

    def get_rec_name(self, name):
        if self.party and self.shipment_address:
            return '%s %s %s' % (
                self.type_.rec_name, self.party.rec_name,
                self.shipment_address.rec_name)
        if self.party:
            return '%s %s' % (self.type_.rec_name, self.party.rec_name)
        return self.type_.rec_name

    @fields.depends('type_')
    def on_change_type_(self):
        if self.type_:
            self.distribution_method = self.type_.distribution_method
            self.formula = self.type_.formula

    @classmethod
    def get_templates(cls, sale):
        type_ids = []
        values = []
        patterns = cls._get_pattern_values(sale)

        # Convert pattern values on dict of priorities
        priorities = {e: v for e, v in enumerate(
            cls._get_pattern(sale))}

        for priority in range(len(priorities)):
            _domain = [(k, '=', v if k in priorities[priority] else None)
                for k, v in patterns.items()
                if k != 'products']
            _domain.append(('type_', 'not in', type_ids))
            with Transaction().set_context(_check_access=False):
                # avoid rules, they can do matching fail
                # (i.e. products of cost type)
                templates = cls.search(_domain)
                match_pattern = {'products': patterns['products']}
                templates = [template for template in templates
                    if template.type_.match(match_pattern)]
                if templates:
                    values.extend(templates)
                    type_ids.extend(list(set([t.type_.id for t in templates])))
        return values

    @classmethod
    def _get_pattern(cls, sale):
        # Return a list of lists with patterns ordered by priority
        keys = [k for k in cls._get_pattern_values(sale).keys()
            if k != 'products']
        res = [
            keys,
            ['party', 'shipment_party'],
            ['party', 'shipment_address'],
            ['party'],
            []
        ]
        if len(keys) > 3:
            res.insert(1, ['party', 'shipment_party', 'shipment_address'])
        return res

    @classmethod
    def _get_pattern_values(cls, sale):
        values = {
            'party': sale.party.id if sale.party else None,
            'shipment_address': (sale.shipment_address.id
                if sale.shipment_address else None),
            'shipment_party': (sale.shipment_party.id
                if sale.shipment_party else None)
        }
        values['products'] = set()
        for line in sale.lines:
            values['products'] |= line._valid_products_for_cost()
        return values


class SaleCost(SaleCostApplyMethodMixin, ModelSQL, ModelView, cost_document()):
    """Cost of sale"""
    __name__ = 'sale.cost'

    _advanced_cost_state = ['confirmed', 'processing', 'done']

    document = fields.Many2One('sale.sale', 'Sale', required=True,
        states={
            'readonly': Bool(Eval('lines_edit')) | Bool(Eval('invoice_lines'))
        })
    type_ = fields.Many2One('sale.cost.type', 'Type', required=True,
        ondelete='RESTRICT',
        domain=[
            If(~Eval('template'), ('manual', '=', True), ()),
            If(And(Eval('sale_state').in_(_advanced_cost_state),
                Eval('id', -1) < 0), ('apply_method', '!=', 'sale'), ()),
            If(And(
                    Not(Equal(Eval('sale_invoice_state'), 'none')),
                    Eval('id', -1) < 0),
                ('apply_method', 'not in', ['sale', 'sale_invoice']),
                ()
            )
        ],
        states={
            'readonly': Bool(Eval('template')) | And(
                Eval('sale_state').in_(_advanced_cost_state),
                Eval('id', -1) > 0)})
    template = fields.Many2One('sale.cost.template', 'Template',
        states={'readonly': Bool(True)})
    lines = fields.One2Many('sale.cost.line', 'cost', 'Lines', readonly=True)
    sale_state = fields.Function(fields.Selection('get_sale_states',
        'Sale State'), 'on_change_with_sale_state',
        searcher='search_sale_data')
    sale_invoice_state = fields.Function(
        fields.Selection('get_sale_invoice_states', 'Sale invoice state'),
        'on_change_with_sale_invoice_state', searcher='search_sale_data')
    comments = fields.Text('Comments', translate=True)
    lines_edit = fields.Boolean('Edit lines')
    sale_date = fields.Function(fields.Date('Sale Date'),
        'get_sale_date', searcher='search_sale_data')
    description = fields.Char('Description',
        states={'readonly': And(
            Equal(Eval('sale_state'), 'done'),
            Equal(Eval('apply_method'), 'sale'))
        })
    invoice_lines = fields.One2Many('account.invoice.line', 'origin',
        'Invoice lines', readonly=True,
        states={
            'invisible': (Eval('apply_method', 'none') != 'sale_invoice')
        },
        context={
            'standalone': True
        })
    undistributed = fields.Function(fields.Boolean('Undistributed'),
        'get_undistributed', searcher='search_undistributed')
    applied = fields.Function(fields.Boolean('Applied'),
        'get_applied', searcher='search_applied')
    appliable = fields.Function(fields.Boolean('Appliable by user'),
        'get_appliable')
    sale_party = fields.Function(
        fields.Many2One('party.party', 'Sale Party'),
        'on_change_with_sale_party', searcher='search_sale_data')
    sale_shipment_party = fields.Function(
        fields.Many2One('party.party', 'Sale Shipment Party'),
        'on_change_with_sale_shipment_party', searcher='search_sale_data')

    @classmethod
    def __setup__(cls):
        super(SaleCost, cls).__setup__()
        table = cls.__table__()
        cls.distribution_method.selection.append(('none', 'None'))
        cls._undistribute_modify = {'amount', 'distribution_method'}
        _readonly_condition = And(Eval('sale_state').in_(
            cls._advanced_cost_state),
            Equal(Eval('apply_method'), 'sale'))
        for field_name in ('formula', 'amount'):
            _field = getattr(cls, field_name)
            if _field.states.get('readonly'):
                _field.states['readonly'] |= _readonly_condition
            else:
                _field.states['readonly'] = _readonly_condition

        cls._buttons.update({
            'apply': {
                'invisible': Not(Eval('appliable')) | Eval('applied'),
                'icon': 'tryton-launch',
                'depends': ['appliable', 'applied']
            },
            'unapply': {
                'invisible': Not(Eval('applied')),
                'icon': 'tryton-launch',
                'depends': ['applied'],
            },
            'compute_amount': {
                'invisible': (
                    Not(Bool(Eval('formula')))
                    | Not(Bool(Eval('compute_formula')))
                ),
                'icon': 'tryton-refresh',
                'depends': ['compute_formula', 'formula']
            }})

        cls._sql_indexes.update({
            Index(table, (table.document, Index.Equality())),
            Index(table, (table.type_, Index.Equality()))
            })

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)

        if table.column_exist('sale'):
            table.column_rename('sale', 'document')
        super(SaleCost, cls).__register__(module_name)

    def get_rec_name(self, name):
        return '%s @ %s' % (self.type_.rec_name, self.document.rec_name)

    @property
    def sale(self):
        return self.document

    @classmethod
    def get_sale_states(cls):
        Sale = Pool().get('sale.sale')
        return Sale.fields_get(['state'])['state']['selection']

    @fields.depends('document', '_parent_document.state')
    def on_change_with_sale_state(self, name=None):
        if self.document:
            return self.document.state

    @fields.depends('document', '_parent_document.party')
    def on_change_with_sale_party(self, name=None):
        if self.document:
            return self.document.party.id

    @fields.depends('document', '_parent_document.shipment_party')
    def on_change_with_sale_shipment_party(self, name=None):
        if self.document and self.document.shipment_party:
            return self.document.shipment_party.id

    @classmethod
    def search_sale_data(cls, name, clause):
        _name = clause[0]
        if name != 'sale_date' and name.startswith('sale_'):
            _name = _name[5:]
        return [
            ('document.%s' % _name, ) + tuple(clause[1:])]

    @classmethod
    def get_sale_invoice_states(cls):
        Sale = Pool().get('sale.sale')
        return Sale.fields_get(['invoice_state'])['invoice_state']['selection']

    @fields.depends('document', '_parent_document.invoice_state')
    def on_change_with_sale_invoice_state(self, name=None):
        if self.document:
            return self.document.invoice_state

    @classmethod
    def get_sale_date(cls, records, name):
        Sale = Pool().get('sale.sale')
        sale = Sale.__table__()
        cost = cls.__table__()
        cursor = Transaction().connection.cursor()

        res = {record.id: None for record in records}
        cursor.execute(*cost.join(sale,
                condition=(cost.document == sale.id)
            ).select(
                cost.id,
                sale.sale_date,
                where=reduce_ids(cost.id, list(res.keys()))))
        res.update({x: y for x, y in cursor.fetchall()})
        return res

    def _order_sale_field(name):
        def order_field(tables):
            pool = Pool()
            Sale = pool.get('sale.sale')
            field = Sale._fields[name]
            table, _ = tables[None]
            sale_tables = tables.get('sale')
            if sale_tables is None:
                sale = Sale.__table__()
                sale_tables = {
                    None: (sale, sale.id == table.document),
                    }
                tables['sale'] = sale_tables
            return field.convert_order(name, sale_tables, Sale)
        return staticmethod(order_field)
    order_sale_date = _order_sale_field('sale_date')
    order_sale_state = _order_sale_field('state')

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('document', ) + tuple(clause[1:]),
                ('type_', ) + tuple(clause[1:])]

    @fields.depends('template', 'type_', '_parent_type_.apply_method',
        '_parent_type_.distribution_method', '_parent_type_.formula',
        methods=['on_change_formula'])
    def on_change_type_(self):
        if not self.type_:
            return
        self.apply_method = self.type_.apply_method
        if not self.template:
            self.distribution_method = self.type_.distribution_method
            self.formula = self.type_.formula
            self.on_change_formula()

    @fields.depends('template', methods=['on_change_formula'])
    def on_change_template(self):
        if self.template:
            self.type_ = self.template.type_
            self.apply_method = self.type_.apply_method
            self.distribution_method = self.template.distribution_method
            self.formula = self.template.formula
            self.on_change_formula()

    @fields.depends('document', '_parent_document.currency')
    def on_change_document(self):
        if self.document:
            self.currency = self.document.currency
            self.currency_digits = self.currency.digits

    @fields.depends('template', 'type_', '_parent_document.id')
    def on_change_formula(self):
        super(SaleCost, self).on_change_formula()

    @fields.depends('sale_state', 'apply_method')
    def get_compute_formula(self, name=None):
        res = super().get_compute_formula(name)
        customer_applied = bool(
            self.apply_method == 'sale'
            and self.sale_state in ('confirmed', 'processing', 'done'))
        return bool(res and not customer_applied)

    @fields.depends('lines_edit', methods=['_distribute'])
    def on_change_lines_edit(self):
        self.lines, _ = self._distribute()

    @classmethod
    def get_document_quantity(cls, records, name=None):
        pool = Pool()
        Uom = pool.get('product.uom')

        values = {r.id: 0 for r in records}
        document_lines = {}
        for record in records:
            document_lines.setdefault(record, []).extend(
                record._get_sale_lines())
        for record, lines in document_lines.items():
            base_uom = cls._get_base_uom(lines)
            if base_uom:
                qty = sum(Uom.compute_qty(line.unit, line.quantity,
                    base_uom) or 0 for line in lines)
                values[record.id] = qty
        return values

    @fields.depends('amount', 'distribution_method', 'document', 'lines',
        methods=['_get_line', '_get_sale_lines'])
    def _distribute(self, use_factor=False):
        if self.amount is None or self.amount == 0 or \
                not self.distribution_method or \
                self.distribution_method == 'none' or \
                not self.document:
            return [], list(self.lines)

        remaining_amount = self.amount
        if use_factor:
            # compute amount base on edited factors
            total_factor = sum(line.factor or 0 for line in self.lines)
            if total_factor:
                for line in self.lines:
                    line.amount = self.currency.round(Decimal(
                        line.factor / total_factor) * self.amount)
                    remaining_amount -= line.amount
                if remaining_amount:
                    line.amount = self.currency.round(
                        line.amount + remaining_amount)
            return list(self.lines), []

        lines = []
        todel = []
        current_ids = [line.document_line.id for line in self.lines]
        sale_lines = self._get_sale_lines()
        for line in sale_lines:
            if line.cost:
                continue
            if line.type != 'line':
                continue
            new_line = self._get_line(line)
            old_line = [oldline for oldline in self.lines
                if oldline.document_line.id == line.id]
            if new_line and not old_line:
                lines.append(new_line)
            elif old_line:
                duplicated_lines = old_line[1:]
                if duplicated_lines:
                    todel.extend(duplicated_lines)
                old_line = old_line[0]
                if new_line and new_line.amount != old_line.amount:
                    old_line.amount = new_line.amount
                    old_line.factor = new_line.factor
                    lines.append(old_line)
                else:
                    lines.append(old_line)
            current_ids = [_id for _id in current_ids if _id != line.id]

        if current_ids:
            todel.extend([line for line in self.lines
                if line.document_line.id in current_ids])

        if lines and all(l.amount is not None for l in lines):
            for line in lines:
                remaining_amount -= line.amount
            if remaining_amount:
                line.amount = self.currency.round(
                    line.amount + remaining_amount)
        return lines, todel

    @dualmethod
    def distribute(cls, records):
        pool = Pool()
        CostLine = pool.get('sale.cost.line')

        to_delete = []
        lines = []
        for record in records:
            rlines, rdlines = record._distribute(use_factor=record.lines_edit)
            if rlines:
                lines.extend(rlines)
            if rdlines:
                to_delete.extend(rdlines)

        if to_delete:
            CostLine.delete(to_delete)
        if lines:
            CostLine.save(lines)

    @dualmethod
    def undistribute(cls, records):
        pool = Pool()
        CostLine = pool.get('sale.cost.line')

        lines = [line for r in records for line in r._get_sale_lines()]
        to_delete = CostLine.search([
            ('cost.lines_edit', '=', False),
            ('document_line', 'in', lines)
        ])
        if to_delete:
            CostLine.delete(to_delete)

    @classmethod
    def _apply_method(cls, apply_method, costs):
        if apply_method == 'sale':
            cls.create_customer_lines(costs)

    @classmethod
    def _unapply_method(cls, apply_method, costs):
        Sale = Pool().get('sale.sale')
        if apply_method == 'sale':
            Sale.delete_cost_lines(list(set(c.sale for c in costs)))

    def get_sale_line(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        if self.type_.apply_method != 'sale':
            return None
        if self.amount == 0:
            return None
        product = self.type_.product
        sequence = None
        if self.document.lines:
            last_line = self.document.lines[-1]
            if last_line.sequence is not None:
                sequence = last_line.sequence + 1

        sale_line = SaleLine(
            sale=self.document,
            sequence=sequence,
            type='line',
            product=product,
            quantity=self.quantity,
            unit=product.sale_uom,
            cost=self,
            )
        sale_line.on_change_product()
        digits = SaleLine.unit_price.digits[1]
        sale_line.unit_price = - (self.amount / Decimal(str(self.quantity))
            ).quantize(Decimal(10) ** -Decimal(digits))
        return sale_line

    @classmethod
    def create_customer_lines(cls, records):
        pool = Pool()
        Sale = pool.get('sale.sale')

        sale_lines = []
        lines = []
        sale_states = {}
        sales = []
        to_write = []
        for record in records:
            if record.document in sales:
                continue
            sales.append(record.document)
            sale_states.setdefault(record.document.state,
                []).append(record.document)
            lines.extend(list(record.document.lines))

        # Change sale status, to avoid core access error
        Sale.write(sales, {'state': 'draft'})

        for cost in records:
            sale_line = cost.get_sale_line()
            if not sale_line:
                continue
            if not any(l.type == 'line'
                    and l.cost.id == sale_line.cost.id
                    and l.unit_price == sale_line.unit_price
                    for l in lines if l.cost):
                sale_line.save()
                sale_lines.append(sale_line)

        # Return to original status
        for key, sales in sale_states.items():
            to_write.extend([sales, {'state': key}])
        if to_write:
            Sale.write(*to_write)

        return sale_lines

    @fields.depends('currency', 'distribution_method', 'amount', 'lines_edit')
    def _get_line(self, sale_line):
        pool = Pool()
        CostLine = pool.get('sale.cost.line')

        if sale_line.type != 'line':
            return None
        line = CostLine(
            cost=self,
            document_line=sale_line,
            factor=None,
            amount=None
        )
        line.on_change_document_line()
        factor = getattr(line, 'document_%s' % self.distribution_method, 0
            ) or 0
        if self.lines_edit:
            line.factor = float(factor)
        elif Decimal(getattr(
                    self, 'document_%s' % self.distribution_method, 0)) != 0:
            line.amount = self.currency.round(
                (Decimal(factor) / Decimal(getattr(
                    self, 'document_%s' % self.distribution_method, 0))
                ) * self.amount)
        return line

    @fields.depends('document', '_parent_document.lines',
        methods=['_valid_line_for_cost'])
    def _get_sale_lines(self):
        return [line for line in self.document.lines
            if self._valid_line_for_cost(line)]

    @classmethod
    def validate(cls, records):
        super(SaleCost, cls).validate(records)
        for record in records:
            if record.distribution_method != 'quantity':
                continue
            if not record.amount:
                continue
            lines = record._get_sale_lines()
            uomcat_ids = set(line.unit.category.id for line in lines)
            if len(uomcat_ids) > 1:
                raise UserError(gettext(
                    'document_cost.msg_document_cost_many_uom_categories',
                    document=record.sale.rec_name))

    @classmethod
    def delete(cls, records):
        for record in records:
            if record.document.state in cls._advanced_cost_state:
                if record.apply_method == 'sale':
                    raise UserError(gettext(
                        'sale_cost.msg_sale_cost_delete_sale_state_apply',
                        cost=record.rec_name))
                if not record.type_.manual:
                    raise UserError(gettext(
                        'sale_cost.msg_sale_cost_delete_sale_state_manual',
                        cost=record.rec_name))
        cls.unapply(records)
        super().delete(records)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        to_undistribute = []

        for records, values in zip(actions, actions):
            vals_set = set(values)
            # undistribute if some fields are modified
            if cls._undistribute_modify & vals_set:
                for record in records:
                    if record.document.state in cls._advanced_cost_state or \
                            record.lines_edit:
                        to_undistribute.append(record)
            args.extend((records, values))

        super().write(*args)

        if to_undistribute:
            cls.undistribute(to_undistribute)
            cls.__queue__.distribute(to_undistribute)

    @fields.depends(methods=['get_document_untaxed_amount',
        'get_document_total_amount', 'get_document_quantity'])
    def _compute_document_values(self):
        super()._compute_document_values()
        self.document_untaxed_amount = self.get_document_untaxed_amount()
        self.document_total_amount = self.get_document_total_amount()
        self.document_quantity = self.get_document_quantity(
            [self])[self.id]

    @property
    def non_explodable(self):
        return False

    @fields.depends('type_')
    def _must_check_lines(self):
        return bool(super()._must_check_lines() or (
            self.type_ and self.type_.products))

    @fields.depends('type_', methods=['_valid_line_for_cost_products'])
    def _valid_line_for_cost(self, line):
        valid = bool(line.type == 'line' and not line.cost)
        if valid and self.type_ and self.type_.products:
            products = self._valid_line_for_cost_products(line)
            valid &= bool(products & set([p.id for p in self.type_.products]))
        return valid

    def _valid_line_for_cost_products(self, line):
        return line._valid_products_for_cost()

    @classmethod
    @ModelView.button
    def compute_amount(cls, records):
        for record in records:
            record.on_change_formula()
            record.save()

    @classmethod
    def search_undistributed(cls, name, clause):
        pool = Pool()
        sql_table = cls.__table__()
        CostLine = pool.get('sale.cost.line')
        Sale = pool.get('sale.sale')
        Currency = pool.get('currency.currency')
        cost_line = CostLine.__table__()
        sale = Sale.__table__()
        currency = Currency.__table__()

        query = sql_table.join(sale, condition=(sale.id == sql_table.document)
            ).join(currency, condition=(currency.id == sale.currency)
            ).join(cost_line, condition=(
                sql_table.id == cost_line.cost)
            ).select(
                sql_table.id,
                where=(
                    (sql_table.distribution_method != 'none')),
                group_by=(sql_table.id, currency.digits, currency.rounding),
                having=(
                    Abs((Coalesce(Sum(Round(
                        Coalesce(cost_line.amount, 0), currency.digits)),
                        0) - Coalesce(sql_table.amount, 0))
                    ) > currency.rounding
                )
        )
        no_lines_query = sql_table.join(cost_line, 'LEFT',
                condition=(sql_table.id == cost_line.cost)
            ).select(
                sql_table.id,
                where=(
                    (sql_table.distribution_method != 'none')
                    & (Coalesce(sql_table.amount, 0) != 0)
                    & (cost_line.id == Null)
                ),
                group_by=sql_table.id
        )
        query = Union(query, no_lines_query, all_=True)

        _, operator, value = clause
        condition = 'in'
        if (operator == '=' and not value) or (operator == '!=' and value):
            condition = 'not in'

        return [('id', condition, query)]

    @classmethod
    def get_undistributed(cls, records, name=None):
        _, _, query = cls.search_undistributed('undistributed',
            ('undistributed', '=', True))[0]

        res = {r.id: False for r in records}
        cursor = Transaction().connection.cursor()
        cursor.execute(*query.select(
            query.id,
            where=(reduce_ids(query.id, list(map(int, records))))
        ))
        values = cursor.fetchall()
        res.update({x[0]: True for x in values})
        return res

    @fields.depends('document', '_parent_document.lines')
    def on_change_with_document_unit_digits(self, name=None):
        return super().on_change_with_document_unit_digits(name=name)

    def get_applied(self, name=None):
        states = list(self._advanced_cost_state)
        states.remove('confirmed')
        return (self.sale_state in states
            and self.apply_method in ['sale', 'sale_invoice'])

    @classmethod
    def search_applied(cls, name, clause):
        join_operator, operator = 'AND', 'in'
        if ((clause[1] == '!=' and clause[2])
                or (clause[1] == '=' and not clause[2])):
            join_operator, operator = 'OR', 'not in'

        states = list(cls._advanced_cost_state)
        states.remove('confirmed')
        return [
            'OR' if join_operator == 'AND' else 'AND',
            [join_operator,
                ('sale_state', operator, states),
                ('apply_method', operator, ['sale', 'sale_invoice'])
            ]
        ]

    def get_appliable(self, name=None):
        return not (self.sale_state == 'cancelled'
            or self.apply_method in ['none', 'sale', 'sale_invoice'])


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    costs = fields.One2Many('sale.cost', 'document', 'Costs',
        states={'readonly': Eval('state') == 'cancelled'})
    untaxed_amount_precost_cache = Monetary('Untaxed before cost Cache',
        currency='currency', digits='currency', readonly=True,
        states={'invisible': ~Eval('cost_amount')})
    total_amount_precost_cache = Monetary('Total amount before cost Cache',
        currency='currency', digits='currency', readonly=True)
    untaxed_amount_precost = fields.Function(
        Monetary('Untaxed amount before cost',
            currency='currency', digits='currency',
            states={'invisible': ~Eval('cost_amount')},),
        'get_untaxed_amount_precost')
    untaxed_amount_postcost = fields.Function(
        Monetary('Untaxed amount after cost',
            currency='currency', digits='currency',
            states={'invisible': ~Eval('cost_amount')}),
        'get_untaxed_amount_postcost')
    sale_cost_amount = fields.Function(
        Monetary('Sale cost amount', currency='currency', digits='currency'),
        'get_cost_amount')
    indirect_cost_amount = fields.Function(
        Monetary('Indirect cost amount',
            currency='currency', digits='currency'),
        'get_cost_amount')
    cost_amount = fields.Function(
        Monetary('Total cost amount', currency='currency', digits='currency'),
        'get_cost_amount')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
            'explode_costs': {
                'invisible': ~Eval('state').in_(['draft', 'quotation']),
                'depends': ['state']
            },
            'distribute_costs': {
                'invisible': Eval('state').in_(['cancelled', 'draft']),
                'depends': ['state']
            }
        })

    @classmethod
    def delete(cls, records):
        pool = Pool()
        Cost = pool.get('sale.cost')

        costs = [c for r in records for c in r.costs]
        if costs:
            with Transaction().set_context(_check_access=False):
                Cost.delete(costs)
        super().delete(records)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['costs'] = None
        res = super(Sale, cls).copy(records, default=default)
        cls.delete_cost_lines(res)
        return cls.browse(list(map(int, res)))

    def get_untaxed_amount_precost(self, name=None):
        if any(l.cost for l in self.lines):
            return self.untaxed_amount + self.sale_cost_amount
        return self.untaxed_amount

    def get_untaxed_amount_postcost(self, name=None):
        if any(l.cost for l in self.lines):
            return self.untaxed_amount
        return self.untaxed_amount - self.sale_cost_amount

    @classmethod
    def get_cost_amount(cls, sales, names):
        sale_amount = {sale.id: Decimal('0.0') for sale in sales}
        indirect_amount = {sale.id: Decimal('0.0') for sale in sales}
        total_amount = {sale.id: Decimal('0.0') for sale in sales}

        for sale in sales:
            for cost in sale.costs:
                amount = cost.amount or Decimal('0.0')
                if cost.apply_method == 'sale':
                    sale_amount[sale.id] += amount
                else:
                    indirect_amount[sale.id] += amount
                total_amount[sale.id] += amount

        result = {
            'sale_cost_amount': sale_amount,
            'indirect_cost_amount': indirect_amount,
            'cost_amount': total_amount,
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @ModelView.button_change('party', 'shipment_party', 'lines', 'id',
        'shipment_address', 'costs', 'untaxed_amount_precost_cache',
        'total_amount_precost_cache')
    def explode_costs(self):
        pool = Pool()
        Template = pool.get('sale.cost.template')
        Cost = pool.get('sale.cost')

        costs = self._get_non_explodable_costs()
        curr_templates = [c.template.id for c in costs if c.template]
        templates = Template.get_templates(self)
        if not templates:
            return
        for template in templates:
            if template.id in curr_templates:
                continue
            cost = Cost(
                document=self,
                template=template)
            cost.on_change_document()
            cost.on_change_template()
            costs.append(cost)

        self.costs = costs

    @classmethod
    def explode_sale_costs(cls, sales):
        for sale in sales:
            sale.explode_costs()
        cls.save(sales)

    def _get_non_explodable_costs(self):
        return [c for c in self.costs if c.non_explodable]

    @classmethod
    def quote(cls, sales):
        Cost = Pool().get('sale.cost')

        to_explode = []
        to_delete = []
        to_compute = []
        use_queue = Transaction().context.get('use_queue', True)
        for sale in sales:
            if sale.costs and sale.state == 'draft':
                to_compute.extend([c for c in sale.costs
                    if c.must_compute_formula
                    and c.amount_formula_computable])
            if not sale.costs and sale.state == 'draft':
                # Sometimes we need explode cost at the moment,
                # so we use a context key
                if not use_queue:
                    sale.explode_costs()
                to_explode.append(sale)
            if sale.state == 'confirmed':
                to_delete.append(sale)
        if to_explode:
            if use_queue:
                with Transaction().set_context(queue_name='sale'):
                    cls.__queue__.explode_sale_costs(to_explode)
            else:
                cls.save(to_explode)

        if to_compute:
            Cost.compute_amount(to_compute)
        for sale in to_delete:
            skip_costs = sale._get_non_explodable_costs()
            Cost.unapply([c for c in sale.costs if c not in skip_costs])
            cls.undistribute_costs(list(sale.costs))
        super(Sale, cls).quote(sales)

    @classmethod
    def draft(cls, sales):
        Cost = Pool().get('sale.cost')

        to_delete = []
        for sale in sales:
            if sale.state == 'confirmed':
                to_delete.append(sale)

        super(Sale, cls).draft(sales)

        for sale in to_delete:
            skip_costs = sale._get_non_explodable_costs()
            Cost.unapply([c for c in sale.costs if c not in skip_costs])
            cls.undistribute_costs(list(sale.costs))

    @classmethod
    def confirm(cls, sales):
        Cost = Pool().get('sale.cost')

        to_check = [s for s in sales if s.state == 'quotation']
        if to_check:
            cls.store_cache_before_costs(to_check)
            for sale in to_check:
                Cost.apply([c for c in sale.costs
                    if c.type_.apply_point == 'on_confirm'])
            cls.save(to_check)
        super(Sale, cls).confirm(sales)
        with Transaction().set_context(queue_name='sale'):
            cls.__queue__.distribute_costs(sales)

    @classmethod
    def store_cache_before_costs(cls, sales):
        for sale in sales:
            if not sale.costs:
                continue
            if any(l.cost for l in sale.lines):
                raise UserError(gettext(
                    'sale_cost.msg_sale_sale_cache_pre_cost',
                    sale=sale.rec_name))
            cls.write([sale], {
                    'untaxed_amount_precost_cache': sale.untaxed_amount,
                    'total_amount_precost_cache': sale.total_amount,
                    })

    @classmethod
    def undistribute_costs(cls, costs):
        with Transaction().set_context(_check_access=False):
            for cost in costs:
                cost.undistribute()

    @classmethod
    def distribute_costs(cls, records):
        pool = Pool()
        Cost = pool.get('sale.cost')

        with Transaction().set_context(_check_access=False):
            costs = [c for r in records for c in r.costs]
            Cost.distribute(costs)

    @classmethod
    def cron_distribute_costs(cls):
        Cost = Pool().get('sale.cost')
        records = Cost.search([
            ('document.state', '!=', 'cancelled'),
            ('undistributed', '=', True)
        ])
        if records:
            Cost.distribute(records)

    @classmethod
    def delete_cost_lines(cls, records):
        pool = Pool()
        Line = pool.get('sale.line')

        lines = Line.search([
            ('sale', 'in', list(map(int, records))),
            ('cost', '!=', None)])
        if lines:
            Line.delete(lines)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    cost = fields.Many2One('sale.cost', 'Cost')
    distributed_costs = fields.One2Many('sale.cost.line', 'document_line',
        'Costs', readonly=True)
    costs_amount = fields.Function(
        fields.Numeric('Costs amount',
            digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2))),
        'get_costs_amount')
    indirect_costs_amount = fields.Function(
        fields.Numeric('Indirect Costs amount',
            digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2))),
        'get_indirect_costs_amount')
    costs_unit_price = fields.Function(
        fields.Numeric('Costs unit price', digits=price_digits),
        'get_costs_unit_price')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['distributed_costs'] = None
        return super(SaleLine, cls).copy(records, default=default)

    @classmethod
    def get_costs_amount(cls, records, names):
        pool = Pool()
        Line = pool.get('sale.cost.line')
        cursor = Transaction().connection.cursor()
        line = Line.__table__()

        columns = {
            'costs_amount': 1,
            'costs_abs_amount': 2,
        }
        record_ids = {r.id: Decimal('0.0') for r in records}
        currencies = {r.id: r.sale.currency for r in records}
        query = line.select(
            line.document_line,
            Sum(line.amount),
            Sum(Abs(line.amount)),
            where=(
                reduce_ids(line.document_line, list(record_ids.keys()))
                & (line.amount != Null)),
            group_by=line.document_line
        )

        cursor.execute(*query)
        values = cursor.fetchall()

        res = {}
        for name in names:
            res.setdefault(name, record_ids.copy())
            res[name].update({
                value[0]: currencies[value[0]].round(
                    Decimal(value[columns[name]])) for value in values
                })
        return res

    def get_indirect_costs_amount(self, name=None):
        amount = Decimal('0.0')
        for cost_line in self.distributed_costs:
            if cost_line.cost.type_.apply_method == 'sale':
                continue
            amount += (cost_line.amount or Decimal('0.0'))
        return self.sale.currency.round(amount)

    @classmethod
    def _get_costs_price(cls, records, name, absolute=False):
        names = absolute and ['costs_abs_amount'] or ['costs_amount']
        res = cls.get_costs_amount(records, names)[names[0]]
        digits = price_digits[1]
        for record in records:
            qty = Decimal(str(getattr(record, name, 0) or 0)) or Decimal('0.0')
            if not qty:
                res[record.id] = Decimal('0.0')
            else:
                res[record.id] = (res[record.id] / qty).quantize(
                    Decimal(10) ** -Decimal(digits))
        return res

    @classmethod
    def get_costs_unit_price(cls, records, name=None):
        return cls._get_costs_price(records, 'quantity')

    @classmethod
    def get_costs_abs_unit_price(cls, records, name=None):
        return cls._get_costs_price(records, 'quantity', absolute=True)

    def _valid_products_for_cost(self):
        if not self.product:
            return set()
        products = set([self.product.id])
        if self.moves:
            products = set([m.product.id for m in self.moves])
        return products


class CostLineSale(SaleCostApplyMethodMixin, ModelSQL, ModelView,
        cost_document_line()):
    """Cost of sale line"""
    __name__ = 'sale.cost.line'

    cost = fields.Many2One('sale.cost', 'Cost', required=True,
        ondelete='CASCADE')
    company = fields.Function(
        fields.Many2One('company.company', 'Company'),
        'get_company', searcher='search_company')
    document_line = fields.Many2One('sale.line', 'Document line',
        required=True, ondelete='CASCADE',
        domain=[
            ('type', '=', 'line'),
            ('cost', '=', None)
        ])
    type_ = fields.Function(
        fields.Many2One('sale.cost.type', 'Type'), 'get_type_')
    party = fields.Function(
        fields.Many2One('party.party', 'Party'),
        'get_party', searcher='search_document_data')
    factor = fields.Float('Factor', digits=(16, 2),
        states={
            'required': Bool(Eval('_parent_cost', {}
                ).get('lines_edit', False)),
            'readonly': Not(Bool(Eval('_parent_cost', {}
                ).get('lines_edit', False))),
            'invisible': Not(Bool(Eval('_parent_cost', {}
                ).get('lines_edit', False))),
        })
    _document_attr = 'sale'
    _document_date = 'sale_date'

    @classmethod
    def __setup__(cls):
        super(CostLineSale, cls).__setup__()
        cls.amount.readonly = True
        table = cls.__table__()

        cls._sql_indexes.update({
            Index(table, (table.document_line, Index.Equality())),
            Index(table, (table.cost, Index.Equality()))
            })

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)

        if table.column_exist('sale_line'):
            table.column_rename('sale_line', 'document_line')
        super(CostLineSale, cls).__register__(module_name)

    def get_company(self, name):
        return self.document_line.sale.company.id

    @classmethod
    def search_company(cls, name, clause):
        return [('document_line.sale.' + clause[0],) + tuple(clause[1:])]

    @property
    def document(self):
        return self.document_line.sale

    @classmethod
    def get_type_(cls, records, name=None):
        return {r.id: r.cost.type_.id for r in records}

    @classmethod
    def get_party(cls, records, name=None):
        return {r.id: r.document.party.id for r in records}

    @classmethod
    def create(cls, vlist):
        Cost = Pool().get('sale.cost')

        records = super().create(vlist)

        costs = set([r.cost for r in records if r.cost.lines_edit])
        if costs:
            Cost.distribute(list(costs))
        return records

    @classmethod
    def write(cls, *args):
        Cost = Pool().get('sale.cost')

        actions = iter(args)
        args = []
        to_check = []
        for records, values in zip(actions, actions):
            if 'factor' in values:
                to_check.extend(list(set(r.cost for r in records)))
            args.extend((records, values))

        super().write(*args)

        if to_check:
            to_check = Cost.browse(list(set(to_check)))
            Cost.distribute([c for c in to_check if c.lines_edit])


class Sale2(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def unprocess(cls, sales):
        pool = Pool()
        Cost = pool.get('sale.cost')
        InvoiceLine = pool.get('account.invoice.line')

        super().unprocess(sales)
        skip_costs = [c for s in sales for c in s._get_non_explodable_costs()]
        Cost.unapply([c for s in sales for c in s.costs
            if c not in skip_costs])
        invoice_lines = [l for s in sales for c in s.costs
            if c.apply_method == 'sale_invoice' for l in c.invoice_lines]
        if invoice_lines:
            InvoiceLine.delete(invoice_lines)


class ProductCost(ModelSQL):
    """Sale Cost Type - Product"""
    __name__ = 'sale.cost.type-product.product'

    cost_type = fields.Many2One('sale.cost.type', 'Cost type',
        ondelete='CASCADE', required=True)
    product = fields.Many2One('product.product', 'Product',
        ondelete='RESTRICT', required=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()

        cls._sql_indexes.update({
            Index(table, (table.cost_type, Index.Equality())),
            Index(table, (table.product, Index.Equality()))
            })


class SaleCostChangeFormulaStart(ModelView):
    """Sale Cost Change Formula Start"""
    __name__ = 'sale.cost.change_formula.start'

    from_date = fields.Date('From date', required=True)
    to_date = fields.Date('To date',
        domain=[If(Eval('from_date') & Eval('to_date'),
            ('to_date', '>=', Eval('from_date')),
            ()), ])
    update_templates = fields.Boolean('Update templates',
        states={
            'invisible': Eval('context', {}).get(
                'active_model') == 'sale.cost.template'
        })
    companies = fields.Many2Many('company.company', None, None, 'Companies',
        domain=[
            ('id', 'in', Eval('allowed_companies'))
        ])
    allowed_companies = fields.Many2Many('company.company', None, None,
        'Allowed companies')


class SaleCostChangeFormula(Wizard):
    """Sale Cost Change Formula"""
    __name__ = 'sale.cost.change_formula'

    start = StateView('sale.cost.change_formula.start',
        'sale_cost.sale_cost_change_formula_start_view_form',
        [Button('Cancel', 'end', 'trytond-cancel'),
         Button('Modify', 'modify', 'tryton-ok', default=True)])
    modify = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        User = pool.get('res.user')

        user = User(Transaction().user)
        if user.company_filter == 'all':
            company_ids = list(map(int, user.companies))
        else:
            company_ids = [user.company.id]
        return {
            'companies': company_ids,
            'allowed_companies': list(map(int, user.companies))
        }

    def transition_modify(self):
        pool = Pool()
        SaleCost = pool.get('sale.cost')
        SaleCostTemplate = pool.get('sale.cost.template')

        if self.start.update_templates:
            templates = SaleCostTemplate.search([
                ('type_', 'in', self.records)],
                order=[('type_', 'ASC')])
            for template in templates:
                template.formula = template.type_.formula
            SaleCostTemplate.save(templates)

        domain = [
            ('sale_date', '>=', self.start.from_date),
            ('sale_state', '!=', 'cancelled'),
            ('document.company', 'in', self.start.companies),
            ('type_', 'in', self.records),
            ('applied', '=', False)
        ]

        if self.start.to_date:
            domain.append(('sale_date', '<=', self.start.to_date))

        costs = SaleCost.search(domain)
        for cost in costs:
            cost.formula = getattr(cost, 'type_').formula
        SaleCost.compute_amount(costs)
        SaleCost.save(costs)

        return 'end'
