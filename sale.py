# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, In
from trytond.model import fields
from trytond.modules.product import price_digits
from functools import partial
from itertools import groupby


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def create_invoice(self):
        pool = Pool()
        Cost = pool.get('sale.cost')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')

        costs = Cost.search([
            ('document', '=', self.id),
            ('apply_method', '=', 'sale_invoice')])

        invoice = super().create_invoice()

        if not costs or not invoice:
            return invoice

        # ensure cost is distributed
        to_distribute = [c for c in costs if c.type_.use_product_account
            and c.undistributed]
        if to_distribute:
            Cost.distribute(to_distribute)
            costs = Cost.browse(list(map(int, costs)))
        to_save = []
        for cost in costs:
            if cost.type_.use_product_account:
                cost_lines = [l for l in cost.lines]
                keyfunc = partial(self.get_cost_invoice_line_keygroup, invoice)
                cost_lines = sorted(cost_lines, key=keyfunc)
                for key, grouped_cost_lines in groupby(cost_lines,
                        key=keyfunc):
                    grouped_cost_lines = list(grouped_cost_lines)
                    amount = -sum(l.amount for l in grouped_cost_lines)
                    invoice_line = self._get_cost_invoice_line(
                        cost, grouped_cost_lines, amount,
                        default_values=dict(key))
                    to_save.append(invoice_line)
            else:
                amount = -cost.amount
                invoice_line = self._get_cost_invoice_line(cost,
                    cost.lines, amount)
                to_save.append(invoice_line)
        if to_save:
            InvoiceLine.save(to_save)
            invoice.save()
            Invoice.write([invoice], {'lines': [('add', map(int, to_save))]})
            Invoice.update_taxes([invoice])
        return invoice

    @classmethod
    def get_cost_invoice_line_keygroup(cls, invoice, cost_line):
        iline, = [l for l in invoice.lines if l.origin
            and l.origin == cost_line.document_line]
        return (
            ('account', iline.account),
            ('taxes', iline.taxes)
        )

    def _get_cost_invoice_line(self, cost, cost_lines,
            amount, default_values={}):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')

        line_values = {
            'account': None,
            'product': None,
            'taxes': None
        }
        line_values.update(default_values)
        qty = 1
        if cost_lines and cost.quantity_formula_computable:
            qty = sum(l.get_quantity() for l in cost_lines)
        if not qty:
            qty = 1
        iline = InvoiceLine(
            invoice_type='out',
            type='line',
            company=self.company.id,
            currency=self.currency.id,
            party=(self.invoice_party and self.invoice_party.id
                or self.party.id),
            quantity=qty,
            origin=cost,
            **line_values)

        if cost.type_.product:
            iline.product = cost.type_.product
            iline.on_change_product()
            iline.quantity = iline.unit.round(iline.quantity)
        elif default_values.get('account') or cost.type_.account_used:
            iline.description = cost.type_.name
            if not iline.account:
                iline.account = cost.type_.account_used
            if not iline.taxes:
                iline.on_change_account()
        iline.unit_price = (amount / Decimal(str(iline.quantity))).quantize(
                Decimal(10) ** -Decimal(InvoiceLine.unit_price.digits[1]))
        iline.amount = iline.on_change_with_amount()

        return iline


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    amount_with_cost = fields.Function(
        fields.Numeric('Amount with cost',
            digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2)),
            states={
                'invisible': Not(In(Eval('type'), ['line', 'subtotal'])),
            }),
        'get_amount_with_cost')
    unit_price_with_cost = fields.Function(
        fields.Numeric('Unit price with cost', digits=price_digits,
            states={
                'invisible': Eval('type') != 'line',
            }),
        'get_unit_price_with_cost')

    def get_amount_with_cost(self, name):
        cost_lines = self.sale_price_cost_lines
        if not cost_lines:
            return self.amount
        amount_cost = sum(l.amount or Decimal('0.0') for l in cost_lines)
        return self.amount - amount_cost

    def get_unit_price_with_cost(self, name):
        unit_price = self.amount_with_cost / Decimal(self.quantity)
        digits = self.__class__.unit_price_with_cost.digits
        return unit_price.quantize(Decimal(1) / 10 ** digits[1])

    @property
    def sale_price_cost_lines(self):
        return [l for l in self.distributed_costs
            if l.cost.type_.include_on_price]


class Sale2(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_cost_invoice_line(self, cost, cost_lines,
            amount, default_values={}):
        pool = Pool()
        AnalyticAccountEntry = pool.get('analytic.account.entry')

        invoice_line = super()._get_cost_invoice_line(
            cost, cost_lines, amount, default_values=default_values)
        new_entries = AnalyticAccountEntry.copy(
            self.lines[0].analytic_accounts,
            default={
                'origin': None})
        invoice_line.analytic_accounts = new_entries
        return invoice_line


class ModifyHeader(metaclass=PoolMeta):
    __name__ = 'sale.modify_header'

    def transition_modify(self):
        res = super().transition_modify()
        sale = self.get_sale()
        if sale.costs:
            Cost = Pool().get('sale.cost')
            Cost.delete(list(sale.costs))
            sale = self.get_sale()
            sale.explode_costs()
            sale.save()
        return res
